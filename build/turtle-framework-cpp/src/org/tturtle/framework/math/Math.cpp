// Generated by Haxe 4.3.4
#include <hxcpp.h>

#ifndef INCLUDED_Date
#include <Date.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_org_tturtle_framework_math_Math
#include <org/tturtle/framework/math/Math.h>
#endif

HX_DEFINE_STACK_FRAME(_hx_pos_14c362accf0fe3f4_8_new,"org.tturtle.framework.math.Math","new",0x94f2e188,"org.tturtle.framework.math.Math.new","org/tturtle/framework/math/Math.hx",8,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_74_sin,"org.tturtle.framework.math.Math","sin",0x94f6b040,"org.tturtle.framework.math.Math.sin","org/tturtle/framework/math/Math.hx",74,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_91_tan,"org.tturtle.framework.math.Math","tan",0x94f76b89,"org.tturtle.framework.math.Math.tan","org/tturtle/framework/math/Math.hx",91,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_109_sqrt,"org.tturtle.framework.math.Math","sqrt",0xc2e99db8,"org.tturtle.framework.math.Math.sqrt","org/tturtle/framework/math/Math.hx",109,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_124_round,"org.tturtle.framework.math.Math","round",0x34c9a376,"org.tturtle.framework.math.Math.round","org/tturtle/framework/math/Math.hx",124,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_127_random,"org.tturtle.framework.math.Math","random",0xe76139db,"org.tturtle.framework.math.Math.random","org/tturtle/framework/math/Math.hx",127,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_134_min,"org.tturtle.framework.math.Math","min",0x94f222ba,"org.tturtle.framework.math.Math.min","org/tturtle/framework/math/Math.hx",134,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_11_add,"org.tturtle.framework.math.Math","add",0x94e90349,"org.tturtle.framework.math.Math.add","org/tturtle/framework/math/Math.hx",11,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_20_subtract,"org.tturtle.framework.math.Math","subtract",0x527b32ec,"org.tturtle.framework.math.Math.subtract","org/tturtle/framework/math/Math.hx",20,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_34_multiply,"org.tturtle.framework.math.Math","multiply",0xf4f69ffc,"org.tturtle.framework.math.Math.multiply","org/tturtle/framework/math/Math.hx",34,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_48_divide,"org.tturtle.framework.math.Math","divide",0xeee44751,"org.tturtle.framework.math.Math.divide","org/tturtle/framework/math/Math.hx",48,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_62_pow,"org.tturtle.framework.math.Math","pow",0x94f46ec0,"org.tturtle.framework.math.Math.pow","org/tturtle/framework/math/Math.hx",62,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_71_abs,"org.tturtle.framework.math.Math","abs",0x94e9019a,"org.tturtle.framework.math.Math.abs","org/tturtle/framework/math/Math.hx",71,0x89a4510a)
HX_LOCAL_STACK_FRAME(_hx_pos_14c362accf0fe3f4_6_boot,"org.tturtle.framework.math.Math","boot",0xb7ab760a,"org.tturtle.framework.math.Math.boot","org/tturtle/framework/math/Math.hx",6,0x89a4510a)
namespace org{
namespace tturtle{
namespace framework{
namespace math{

void Math_obj::__construct(){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_8_new)
            	}

Dynamic Math_obj::__CreateEmpty() { return new Math_obj; }

void *Math_obj::_hx_vtable = 0;

Dynamic Math_obj::__Create(::hx::DynamicArray inArgs)
{
	::hx::ObjectPtr< Math_obj > _hx_result = new Math_obj();
	_hx_result->__construct();
	return _hx_result;
}

bool Math_obj::_hx_isInstanceOf(int inClassId) {
	return inClassId==(int)0x00000001 || inClassId==(int)0x31f615a2;
}

Float Math_obj::sin(Float x){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_74_sin)
HXLINE(  75)		Float result = ( (Float)(0) );
HXLINE(  76)		Float term = x;
HXLINE(  77)		Float sign = ( (Float)(-1) );
HXLINE(  78)		Float power = x;
HXLINE(  79)		Float factorial = ( (Float)(1) );
HXLINE(  81)		{
HXLINE(  81)			int _g = 1;
HXDLIN(  81)			while((_g < 10)){
HXLINE(  81)				_g = (_g + 1);
HXDLIN(  81)				int i = (_g - 1);
HXLINE(  82)				result = (result + (sign * term));
HXLINE(  83)				power = (power * (x * x));
HXLINE(  84)				factorial = (factorial * ( (Float)(((2 * i) * ((2 * i) + 1))) ));
HXLINE(  85)				term = (term * (power / factorial));
HXLINE(  86)				sign = (sign * ( (Float)(-1) ));
            			}
            		}
HXLINE(  88)		return result;
            	}


HX_DEFINE_DYNAMIC_FUNC1(Math_obj,sin,return )

Float Math_obj::tan(Float x){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_91_tan)
HXLINE(  92)		Float result = ( (Float)(0) );
HXLINE(  93)		Float term = x;
HXLINE(  94)		Float sign = ( (Float)(1) );
HXLINE(  95)		Float prevResult = ( (Float)(0) );
HXLINE(  97)		{
HXLINE(  97)			int _g = 1;
HXDLIN(  97)			while((_g < 100)){
HXLINE(  97)				_g = (_g + 1);
HXLINE(  98)				result = (result + (sign * term));
HXLINE(  99)				sign = sign;
HXLINE( 100)				term = (term * ((x * x) / ( (Float)(((2 * (_g - 1)) + 1)) )));
HXLINE( 102)				if ((::org::tturtle::framework::math::Math_obj::abs((result - prevResult)) < ((Float)0.000001))) {
HXLINE( 102)					goto _hx_goto_3;
            				}
HXLINE( 103)				prevResult = result;
            			}
            			_hx_goto_3:;
            		}
HXLINE( 106)		return result;
            	}


HX_DEFINE_DYNAMIC_FUNC1(Math_obj,tan,return )

Float Math_obj::sqrt(Float x){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_109_sqrt)
HXLINE( 110)		if ((x <= 0)) {
HXLINE( 110)			return ( (Float)(0) );
            		}
HXLINE( 112)		Float guess = (x / ( (Float)(2) ));
HXLINE( 113)		Float prevGuess;
HXLINE( 115)		do {
HXLINE( 116)			prevGuess = guess;
HXLINE( 117)			guess = ((guess + (x / guess)) / ( (Float)(2) ));
            		} while(!((::org::tturtle::framework::math::Math_obj::abs((guess - prevGuess)) < ((Float)0.00001))));
;
HXLINE( 120)		return guess;
            	}


HX_DEFINE_DYNAMIC_FUNC1(Math_obj,sqrt,return )

int Math_obj::round(Float x){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_124_round)
HXDLIN( 124)		return ::Std_obj::_hx_int((x + ((Float)0.5)));
            	}


HX_DEFINE_DYNAMIC_FUNC1(Math_obj,round,return )

Float Math_obj::random(){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_127_random)
HXLINE( 128)		Float seed = ::Date_obj::now()->getTime();
HXLINE( 129)		seed = ( (Float)((((::Std_obj::_hx_int(seed) * (int)1103515245) + 12345) & (int)2147483647)) );
HXLINE( 130)		return (seed / ( (Float)((int)2147483647) ));
            	}


HX_DEFINE_DYNAMIC_FUNC0(Math_obj,random,return )

Float Math_obj::min(Float a,Float b){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_134_min)
HXDLIN( 134)		if ((a > b)) {
HXDLIN( 134)			return a;
            		}
            		else {
HXDLIN( 134)			return b;
            		}
HXDLIN( 134)		return ((Float)0.);
            	}


HX_DEFINE_DYNAMIC_FUNC2(Math_obj,min,return )

Float Math_obj::PI;

Float Math_obj::add(::Array< Float > numbers){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_11_add)
HXLINE(  12)		Float result = ( (Float)(0) );
HXLINE(  13)		{
HXLINE(  13)			int _g_current = 0;
HXDLIN(  13)			while((_g_current < numbers->length)){
HXLINE(  13)				_g_current = (_g_current + 1);
HXLINE(  14)				result = (result + numbers->__get((_g_current - 1)));
            			}
            		}
HXLINE(  17)		return result;
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Math_obj,add,return )

Float Math_obj::subtract(::Array< Float > numbers){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_20_subtract)
HXLINE(  21)		Float result = ( (Float)(0) );
HXLINE(  22)		bool firstNum = true;
HXLINE(  23)		{
HXLINE(  23)			int _g_current = 0;
HXDLIN(  23)			while((_g_current < numbers->length)){
HXLINE(  23)				_g_current = (_g_current + 1);
HXDLIN(  23)				Float number = numbers->__get((_g_current - 1));
HXLINE(  24)				if (firstNum) {
HXLINE(  25)					result = number;
HXLINE(  26)					firstNum = false;
HXLINE(  27)					continue;
            				}
HXLINE(  29)				result = (result - number);
            			}
            		}
HXLINE(  31)		return result;
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Math_obj,subtract,return )

Float Math_obj::multiply(::Array< Float > numbers){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_34_multiply)
HXLINE(  35)		Float result = ( (Float)(0) );
HXLINE(  36)		bool firstNum = true;
HXLINE(  37)		{
HXLINE(  37)			int _g_current = 0;
HXDLIN(  37)			while((_g_current < numbers->length)){
HXLINE(  37)				_g_current = (_g_current + 1);
HXDLIN(  37)				Float number = numbers->__get((_g_current - 1));
HXLINE(  38)				if (firstNum) {
HXLINE(  39)					result = number;
HXLINE(  40)					firstNum = false;
HXLINE(  41)					continue;
            				}
HXLINE(  43)				result = (result * number);
            			}
            		}
HXLINE(  45)		return result;
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Math_obj,multiply,return )

Float Math_obj::divide(::Array< Float > numbers){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_48_divide)
HXLINE(  49)		Float result = ( (Float)(0) );
HXLINE(  50)		bool firstNum = true;
HXLINE(  51)		{
HXLINE(  51)			int _g_current = 0;
HXDLIN(  51)			while((_g_current < numbers->length)){
HXLINE(  51)				_g_current = (_g_current + 1);
HXDLIN(  51)				Float number = numbers->__get((_g_current - 1));
HXLINE(  52)				if (firstNum) {
HXLINE(  53)					result = number;
HXLINE(  54)					firstNum = false;
HXLINE(  55)					continue;
            				}
HXLINE(  57)				result = (result / number);
            			}
            		}
HXLINE(  59)		return result;
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Math_obj,divide,return )

Float Math_obj::pow(Float number,int power){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_62_pow)
HXLINE(  63)		Float starterNumber = number;
HXLINE(  64)		{
HXLINE(  64)			int _g = 0;
HXDLIN(  64)			int _g1 = (power - 1);
HXDLIN(  64)			while((_g < _g1)){
HXLINE(  64)				_g = (_g + 1);
HXLINE(  65)				number = (number * starterNumber);
            			}
            		}
HXLINE(  67)		return number;
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(Math_obj,pow,return )

Float Math_obj::abs(Float number){
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_71_abs)
HXDLIN(  71)		if ((number < 0)) {
HXDLIN(  71)			return -(number);
            		}
            		else {
HXDLIN(  71)			return number;
            		}
HXDLIN(  71)		return ((Float)0.);
            	}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Math_obj,abs,return )


::hx::ObjectPtr< Math_obj > Math_obj::__new() {
	::hx::ObjectPtr< Math_obj > __this = new Math_obj();
	__this->__construct();
	return __this;
}

::hx::ObjectPtr< Math_obj > Math_obj::__alloc(::hx::Ctx *_hx_ctx) {
	Math_obj *__this = (Math_obj*)(::hx::Ctx::alloc(_hx_ctx, sizeof(Math_obj), false, "org.tturtle.framework.math.Math"));
	*(void **)__this = Math_obj::_hx_vtable;
	__this->__construct();
	return __this;
}

Math_obj::Math_obj()
{
}

::hx::Val Math_obj::__Field(const ::String &inName,::hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"sin") ) { return ::hx::Val( sin_dyn() ); }
		if (HX_FIELD_EQ(inName,"tan") ) { return ::hx::Val( tan_dyn() ); }
		if (HX_FIELD_EQ(inName,"min") ) { return ::hx::Val( min_dyn() ); }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"sqrt") ) { return ::hx::Val( sqrt_dyn() ); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"round") ) { return ::hx::Val( round_dyn() ); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"random") ) { return ::hx::Val( random_dyn() ); }
	}
	return super::__Field(inName,inCallProp);
}

bool Math_obj::__GetStatic(const ::String &inName, Dynamic &outValue, ::hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"PI") ) { outValue = ( PI ); return true; }
		break;
	case 3:
		if (HX_FIELD_EQ(inName,"add") ) { outValue = add_dyn(); return true; }
		if (HX_FIELD_EQ(inName,"pow") ) { outValue = pow_dyn(); return true; }
		if (HX_FIELD_EQ(inName,"abs") ) { outValue = abs_dyn(); return true; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"divide") ) { outValue = divide_dyn(); return true; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"subtract") ) { outValue = subtract_dyn(); return true; }
		if (HX_FIELD_EQ(inName,"multiply") ) { outValue = multiply_dyn(); return true; }
	}
	return false;
}

bool Math_obj::__SetStatic(const ::String &inName,Dynamic &ioValue,::hx::PropertyAccess inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"PI") ) { PI=ioValue.Cast< Float >(); return true; }
	}
	return false;
}

#ifdef HXCPP_SCRIPTABLE
static ::hx::StorageInfo *Math_obj_sMemberStorageInfo = 0;
static ::hx::StaticInfo Math_obj_sStaticStorageInfo[] = {
	{::hx::fsFloat,(void *) &Math_obj::PI,HX_("PI",f9,45,00,00)},
	{ ::hx::fsUnknown, 0, null()}
};
#endif

static ::String Math_obj_sMemberFields[] = {
	HX_("sin",18,9f,57,00),
	HX_("tan",61,5a,58,00),
	HX_("sqrt",e0,ab,59,4c),
	HX_("round",4e,f8,65,ed),
	HX_("random",03,22,8f,b7),
	HX_("min",92,11,53,00),
	::String(null()) };

static void Math_obj_sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Math_obj::PI,"PI");
};

#ifdef HXCPP_VISIT_ALLOCS
static void Math_obj_sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Math_obj::PI,"PI");
};

#endif

::hx::Class Math_obj::__mClass;

static ::String Math_obj_sStaticFields[] = {
	HX_("PI",f9,45,00,00),
	HX_("add",21,f2,49,00),
	HX_("subtract",14,75,11,f8),
	HX_("multiply",24,e2,8c,9a),
	HX_("divide",79,2f,12,bf),
	HX_("pow",98,5d,55,00),
	HX_("abs",72,f0,49,00),
	::String(null())
};

void Math_obj::__register()
{
	Math_obj _hx_dummy;
	Math_obj::_hx_vtable = *(void **)&_hx_dummy;
	::hx::Static(__mClass) = new ::hx::Class_obj();
	__mClass->mName = HX_("org.tturtle.framework.math.Math",96,bd,f9,6b);
	__mClass->mSuper = &super::__SGetClass();
	__mClass->mConstructEmpty = &__CreateEmpty;
	__mClass->mConstructArgs = &__Create;
	__mClass->mGetStaticField = &Math_obj::__GetStatic;
	__mClass->mSetStaticField = &Math_obj::__SetStatic;
	__mClass->mMarkFunc = Math_obj_sMarkStatics;
	__mClass->mStatics = ::hx::Class_obj::dupFunctions(Math_obj_sStaticFields);
	__mClass->mMembers = ::hx::Class_obj::dupFunctions(Math_obj_sMemberFields);
	__mClass->mCanCast = ::hx::TCanCast< Math_obj >;
#ifdef HXCPP_VISIT_ALLOCS
	__mClass->mVisitFunc = Math_obj_sVisitStatics;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mMemberStorageInfo = Math_obj_sMemberStorageInfo;
#endif
#ifdef HXCPP_SCRIPTABLE
	__mClass->mStaticStorageInfo = Math_obj_sStaticStorageInfo;
#endif
	::hx::_hx_RegisterClass(__mClass->mName, __mClass);
}

void Math_obj::__boot()
{
{
            	HX_STACKFRAME(&_hx_pos_14c362accf0fe3f4_6_boot)
HXDLIN(   6)		PI = ((Float)3.14159265358979323846);
            	}
}

} // end namespace org
} // end namespace tturtle
} // end namespace framework
} // end namespace math
